CS542 Design Patterns
Fall 2013
PROJECT-3 README FILE

Due Date: Sunday, November 10, 2013
Submission Date: Sunday, November 10, 2013
Grace Period Used This Project: 0 Days
Grace Period Remaining: N/A
Author(s): Sachin Rajput 
e-mail(s): srajput1@binghamton.edu 


PURPOSE:

[
	This assignment helped me to learn the use of reflection library from Java lib.  
	JAVADOC is created: reflection/javadoc/index.html
]


PERCENT COMPLETE:

[
	I believe, we have completed 100% of this project.
]

PARTS THAT ARE NOT COMPLETE:

[
 N/A
]

BUGS:

[
 None
]

FILES:
[
	Driver.java: Main driver code resides in this class
	DeserializeStrategy.java: This interface sets the rules what the deserialization ways have to implement.
	Deserialize.java: This class is one of the way to achieve deserialization.
	DeserializeTypes.java: This class is used to extract and save the data based on the type to achieve deserialization.
	SerializeStrategy.java: This interface sets the rules what the serialization ways have to implement.
	Serialize.java: This class is one of the way to achieve serialization.
	Debug.java: This class is used to print the debug messages.
	FileHelper.java: This class is used to support methods required for file.
	Helper.java: Misc methods for the entire application.
	MyAllTypesFirst.java: The class which is looked-up to when deserializing and serializing.
	MyAllTypesSecond.java: The class which is looked-up to when deserializing and serializing.
]

SAMPLE OUTPUT:
[
 bingsuns2% ant -Darg0=MyAllTypes.txt -Darg1=MyAllTypesCopy.txt -Darg2=0 run
 Buildfile: /import/linux/home/srajput1/dp/reflection/build.xml

jar:

run:
     [java] Unique MyAllTypesFirst=4
     [java] Unique MyAllTypesSecond=9
     [java] 

BUILD SUCCESSFUL
Total time: 0 seconds
]

TO COMPILE:
[
 tar -xvf Rajput_Sachin_assign3.tar 
 cd Rajput_Sachin_assign3
 ant compile
]

TO RUN:
[
   ant run
]

DEBUG LEVELS:
[

 0: Only Unique count is printed.
 1: Only output from the Driver code should be printed
 2: To see error messages if exist
 3: Display the values deserialized from XML text file
 Note: set this in build file - in target name="run" alter <arg value="1" />
]

EXTRA CREDIT:
[
 N/A
]


BIBLIOGRAPHY:
This serves as evidence that we are in no way intending Academic Dishonesty.
<Sachin Rajput>

[
. JAVA API Documentation (http://docs.oracle.com/javase/7/docs/api/)
. Complete Reference - JAVA
]

