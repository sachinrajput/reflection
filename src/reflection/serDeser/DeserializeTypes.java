package reflection.serDeser;

import java.lang.reflect.Method;

import reflection.util.Debug;

/***
 * 
 * @author Sachin Rajput
 *
 */
public class DeserializeTypes {
	private Object obj;
	private Method method;
	
	public DeserializeTypes(){
		Debug.printDebug(1, "Constructor called for class DeserializeTypes");
	}
	
	public void setInvoke(Object objIn, Method methodIn){
		this.obj = objIn;
		this.method = methodIn;
	}
	
	public void invokeint(String type){
		try {
			this.method.invoke(this.obj, Integer.parseInt(type));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokeint(String type)");
		} 
	}
	
	public void invokestring(String type){
		try {
			this.method.invoke(this.obj, type);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokestring(String type)");
		} 
	}
	
	public void invokelong(String type){
		try {
			this.method.invoke(this.obj, Long.parseLong(type));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokelong(String type)");
		} 
	}
	
	public void invokedouble(String type){
		try {
			this.method.invoke(this.obj, Double.parseDouble(type));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokedouble(String type)");
		} 
	}
	
	public void invokeshort(String type){
		try {
			this.method.invoke(this.obj, Short.parseShort(type));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokeshort(String type)");
		} 
	}
	
	public void invokechar(String type){
		try {
			this.method.invoke(this.obj, type.charAt(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokechar(String type)");
		} 
	}
	
	public void invokefloat(String type){
		try {
			this.method.invoke(this.obj, Float.parseFloat(type));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During parsing xsd:"+ type + " in method: invokefloat(String type)");
		} 
	}
}
