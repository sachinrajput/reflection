package reflection.serDeser;


import reflection.util.Debug;
import reflection.util.FileHelper;

import java.io.BufferedReader;
import java.lang.reflect.Method;
import java.util.ArrayList;

import reflection.util.Helper;

/***
 * 
 * @author Sachin Rajput
 *
 */
public class Deserialize implements DeserializeStrategy {

	private String fileName;
	BufferedReader br = null;
	private FileHelper fileHelper;
	
	Helper helper = new Helper();
	
	public Deserialize(){
		Debug.printDebug(1, "Constructor called for class Deserialize");
	}
	
	/***
	 * Method deserializeInput is used to turn the XML text file into class objects with data
	 * @param fileName
	 * @return ArrayList<Object> which will have an array of objects of classes from XML file
	 */
	public ArrayList<Object> deserializeFile(String fileName) {
		
		/**
		 * Let us first Initialize first
		 */
		this.fileName = fileName;
		this.fileHelper = new FileHelper(this.fileName);
		this.br = fileHelper.openFileRead();
		
		String sCurrentLine;
		Boolean objectMode = false;
		Boolean createInstance = false;
		Class<?> cls = null;
		Object obj = null;
		ArrayList<Object> listClasses = new ArrayList<Object>();
		
		int type = 0;

		try {
			while ((sCurrentLine = br.readLine()) != null) {
				if (!createInstance) {
					if (!objectMode)
						objectMode = true;
					else {
						String type_name[] = sCurrentLine.split("\"");
						String type_name_dot[] = type_name[1].split("\\.");
						Debug.printDebug(3, "Create class of type: "
								+ type_name_dot[2]);
						createInstance = true;
						if (type_name_dot[2].equals("MyAllTypesFirst"))
							type = 1;
						else
							type = 2;
						cls = Class.forName("reflection.util." + type_name_dot[2]);
						obj = cls.newInstance();
					}
				} else {
					String search_open[] = sCurrentLine.split("<");
					if (search_open[1].equals("/complexType>")) {
						objectMode = false;
					} else if (search_open[1].equals("/DPSerialization>")) {
						createInstance = false;
						listClasses.add(obj);
						cls = null;
						obj = null;
						type = 0;
						Debug.printDebug(3, "");
					} else {
						String xsi_search[] = search_open[1].split("xsi");
						String colon_search[] = xsi_search[1].split(":");
						String quotes_search[] = colon_search[2].split("\"");
						String data_search[] = quotes_search[1].split(">");

						String dataName = xsi_search[0].trim();
						String dataType = quotes_search[0].trim();
						String data = data_search[1].trim();
						if (obj != null)
							Debug.printDebug(3, "Datamember: " + dataName
									+ "Datatype: " + dataType + " Data : "
									+ data);
						setData(obj, cls, type, dataType, dataName, data);
						
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: During deserializing the XML file to Objects of classes.");
		} finally{
			this.fileHelper.closeFile();
		}
		return listClasses;
	}

	private void setData(Object obj, Class<?> cls, int type, String dataType,
			String dataName, String data) {
		// TODO Auto-generated method stub

		try {
			String methodName;
			Class<?>[] param = new Class[1];
			param[0] = 	helper.find(dataType);
			if(param[0] == null){
				Debug.printDebug(2,"ERROR: Invoking method: set"+dataName+" for setting the value : "+data+" of type : "+dataType);
			}
			methodName = "set" + dataName;
			Method method = cls.getDeclaredMethod(methodName, param);
			
			/* Call the required method depending on the type */
			Class<?> deserializeClass = Class.forName("reflection.serDeser.DeserializeTypes");
			Object objDeser = deserializeClass.newInstance();
			
			Class<?>[] invokeSetParam = new Class[2];
			invokeSetParam[0] = Object.class;
			invokeSetParam[1] = Method.class;
			Method invokeSetMethod = deserializeClass.getDeclaredMethod("setInvoke", invokeSetParam);
			invokeSetMethod.invoke(objDeser, obj, method);
			
			String invokeMethodName = "invoke"+dataType;
			Class<?>[] invokeParam = new Class[1];
			invokeParam[0] = String.class;
			Method invokeMethod = deserializeClass.getDeclaredMethod(invokeMethodName, invokeParam);
			invokeMethod.invoke(objDeser, data);
			
		} catch (Exception e) {
			Debug.printDebug(2, "ERROR: Invoking method: set"+dataName+" for setting the value : "+data+" of type : "+dataType);
		} finally {

		}
	}

	/***
	 * Overriding toString method for Deserialize class
	 */
	@Override
	public String toString() {
		return "Deserialize [fileName=" + fileName + ", br=" + br
				+ ", fileHelper=" + fileHelper + ", helper=" + helper + "]";
	}
	
	

}
