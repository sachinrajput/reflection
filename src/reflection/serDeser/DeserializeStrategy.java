/**
 * 
 */
package reflection.serDeser;

import java.util.ArrayList;

/**
 * @author sachin
 *
 */
public interface DeserializeStrategy {
	
	/***
	 * This is the abstract method which all deserializing algorithms will
	 * implement.
	 * @param fileName
	 */
	public ArrayList<Object> deserializeFile(String fileName);
	
}
