package reflection.serDeser;

import java.io.BufferedWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import reflection.driver.Driver;
import reflection.util.Debug;
import reflection.util.FileHelper;

/***
 * 
 * @author Sachin Rajput
 *
 */
public class Serialize implements SerializeStrategy{

	private Object deserializedObjects;
	private BufferedWriter bwriter;
	private FileHelper fileHelper;
	
	public Serialize(){
		Debug.printDebug(1, "Constructor called for class Serialize");
	}

	public void serializeFile(ArrayList<Object> serializedObjIn){
		try {
			this.setDeserializedObjects(serializedObjIn);

			/**
			 * Let us first create a new file to copy
			 */

			this.fileHelper = new FileHelper(Driver.outputFileName);
			this.bwriter = fileHelper.openFileWrite();
			int i = 0;
			for (Object lisObjects : serializedObjIn) {
				i++;
				String className = lisObjects.getClass().getName();
				//call the reflection way of class instantiation and call all methods 
				try {
					Class<?> createNewInstance = Class.forName(className);
					if(i>1)
						this.bwriter.write("\n<DPSerialization>\n");
					else 
						this.bwriter.write("<DPSerialization>\n");
					this.bwriter.write(" <complexType xsi:type=\""+className+"\">\n");
					
					Field[] fields = createNewInstance.getFields();

					for(Field field : fields){
							String fieldName = field.getName();
							String fieldType = field.getType().getSimpleName().toLowerCase();
							//String fieldValue = field.get(lisObjects).toString();
							
							Method invokeSetMethod = createNewInstance.getDeclaredMethod("get"+fieldName, new Class[0]);
							Object val = null;
							try{
								val = invokeSetMethod.invoke(lisObjects, new Object[0]);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								Debug.printDebug(2, "ERROR: During method invocation : get"+fieldName+"() to get "
										+ "fieldName: "+fieldName+" with fieldType: "+ fieldType);
							}
							
							//System.out.println("field = " + field.getName());
							this.bwriter.write("  <"+fieldName+" xsi:type=\"xsd:"+fieldType+"\">"+val+"</"+fieldName+">\n");
					}
					
					this.bwriter.write(" </complexType>\n");
					this.bwriter.write("</DPSerialization>");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Debug.printDebug(2, "ERROR: During serializing the Objects to XML file with reflection.");
				}
			}
		} catch (Exception e) {
			Debug.printDebug(2, "ERROR: During serializing the Objects to XML file while reading the objects.");
		} finally {
			this.fileHelper.closeFile();
		}
	}

	/**
	 * @return the deserializedObjects
	 */
	public Object getDeserializedObjects() {
		return deserializedObjects;
	}

	/**
	 * @param deserializedObjects the deserializedObjects to set
	 */
	public void setDeserializedObjects(Object deserializedObjects) {
		this.deserializedObjects = deserializedObjects;
	}

	/***
	 * Overriding toString method for Deserialize class
	 */
	@Override
	public String toString() {
		return "Serialize [deserializedObjects=" + deserializedObjects
				+ ", bwriter=" + bwriter + ", fileHelper=" + fileHelper + "]";
	}
	
	
}
