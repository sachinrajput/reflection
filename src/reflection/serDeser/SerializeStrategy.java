/**
 * 
 */
package reflection.serDeser;

import java.util.ArrayList;

/**
 * @author Sachin Rajput
 *
 */
public interface SerializeStrategy {

	/***
	 * This is the abstract method which all deserializing algorithms will
	 * implement.
	 * @param fileName
	 */
	public void serializeFile(ArrayList<Object> serializedObjIn);
}
