package reflection.util;

/***
 * 
 * @author Sachin Rajput
 *
 */
public class MyAllTypesFirst {
	
	public int myInt;
	public String myString;
	public double myDouble;
	public long myLong;
	public char myChar;
	
	/**
	 * @return the myInt
	 */
	public int getmyInt() {
		return myInt;
	}
	/**
	 * @param myInt the myInt to set
	 */
	public void setmyInt(int myInt) {
		this.myInt = myInt;
	}
	/**
	 * @return the myString
	 */
	public String getmyString() {
		return myString;
	}
	/**
	 * @param myString the myString to set
	 */
	public void setmyString(String myString) {
		this.myString = myString;
	}
	/**
	 * @return the myDouble
	 */
	public double getmyDouble() {
		return myDouble;
	}
	/**
	 * @param myDouble the myDouble to set
	 */
	public void setmyDouble(double myDouble) {
		this.myDouble = myDouble;
	}
	/**
	 * @return the myLong
	 */
	public long getmyLong() {
		return myLong;
	}
	/**
	 * @param myLong the myLong to set
	 */
	public void setmyLong(long myLong) {
		this.myLong = myLong;
	}
	/**
	 * @return the myChar
	 */
	public char getmyChar() {
		return myChar;
	}
	/**
	 * @param myChar the myChar to set
	 */
	public void setmyChar(char myChar) {
		this.myChar = myChar;
	}
	
	
	/***
	 * hashCode() overridden to implement the compare functionality
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + myChar;
		long temp;
		temp = Double.doubleToLongBits(myDouble);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + myInt;
		result = prime * result + (int) (myLong ^ (myLong >>> 32));
		result = prime * result
				+ ((myString == null) ? 0 : myString.hashCode());
		return result;
	}
	

	/***
	 * equals(Object obj) overridden to implement the compare functionality
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MyAllTypesFirst))
			return false;
		MyAllTypesFirst other = (MyAllTypesFirst) obj;
		if (myChar != other.myChar)
			return false;
		if (Double.doubleToLongBits(myDouble) != Double
				.doubleToLongBits(other.myDouble))
			return false;
		if (myInt != other.myInt)
			return false;
		if (myLong != other.myLong)
			return false;
		if (myString == null) {
			if (other.myString != null)
				return false;
		} else if (!myString.equals(other.myString))
			return false;
		return true;
	}
		
}
