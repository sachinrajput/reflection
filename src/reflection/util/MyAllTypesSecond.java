package reflection.util;

/***
 * 
 * @author Sachin Rajput
 *
 */
public class MyAllTypesSecond {

	public int myIntS;
	public String myStringS;
	public float myFloatS;
	public short myShortS;
	public char myCharS;

	/**
	 * @return the myIntS
	 */
	public int getmyIntS() {
		return myIntS;
	}

	/**
	 * @param myIntS
	 *            the myIntS to set
	 */
	public void setmyIntS(int myIntS) {
		this.myIntS = myIntS;
	}

	/**
	 * @return the myStringS
	 */
	public String getmyStringS() {
		return myStringS;
	}

	/**
	 * @param myStringS
	 *            the myStringS to set
	 */
	public void setmyStringS(String myStringS) {
		this.myStringS = myStringS;
	}

	/**
	 * @return the myDoubleS
	 */
	public float getmyFloatS() {
		return myFloatS;
	}

	/**
	 * @param myDoubleS
	 *            the myDoubleS to set
	 */
	public void setmyFloatS(float myFloatS) {
		this.myFloatS = myFloatS;
	}

	/**
	 * @return the myLongS
	 */
	public short getmyShortS() {
		return myShortS;
	}

	/**
	 * @param myLongS
	 *            the myLongS to set
	 */
	public void setmyShortS(short myShortS) {
		this.myShortS = myShortS;
	}

	/**
	 * @return the myCharS
	 */
	public char getmyCharS() {
		return myCharS;
	}

	/**
	 * @param myCharS
	 *            the myCharS to set
	 */
	public void setmyCharS(char myCharS) {
		this.myCharS = myCharS;
	}
	

	/***
	 * hashCode() overridden to implement the compare functionality
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + myCharS;
		result = prime * result + Float.floatToIntBits(myFloatS);
		result = prime * result + myIntS;
		result = prime * result + myShortS;
		result = prime * result
				+ ((myStringS == null) ? 0 : myStringS.hashCode());
		return result;
	}

	/***
	 * equals(Object obj) overridden to implement the compare functionality
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MyAllTypesSecond))
			return false;
		MyAllTypesSecond other = (MyAllTypesSecond) obj;
		if (myCharS != other.myCharS)
			return false;
		if (Float.floatToIntBits(myFloatS) != Float
				.floatToIntBits(other.myFloatS))
			return false;
		if (myIntS != other.myIntS)
			return false;
		if (myShortS != other.myShortS)
			return false;
		if (myStringS == null) {
			if (other.myStringS != null)
				return false;
		} else if (!myStringS.equals(other.myStringS))
			return false;
		return true;
	}


	

}
