package reflection.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/***
 * 
 * @author Sachin Rajput
 * 
 */
public class Helper {

	private HashMap<String, Class<?>> inputMapping = new HashMap<String, Class<?>>();
	private HashMap<Integer, String> classMapping = new HashMap<Integer, String>();
	private HashMap<String, Integer> uniqueCount = new HashMap<String, Integer>();

	public Helper() {
		// TODO Auto-generated constructor stub
		setInputMapping();
	}

	/***
	 * find if the datatype exist and give the required param type from hashmap
	 * 
	 * @param dataType
	 * @return
	 */
	public Class<?> find(String dataType) {
		// TODO Auto-generated method stub
		return inputMapping.get(dataType);
	}

	/***
	 * checkUnique method gives you the number of unique objects generated from
	 * the XML file not counting the duplicates.
	 * 
	 * @param serializedObjIn
	 */
	public void checkUnique(ArrayList<Object> serializedObjIn) {
		try {
			int count = serializedObjIn.size();

			for (int i = 0; i < count; i++) {
				String className1 = ((Object) serializedObjIn.get(i))
						.getClass().getSimpleName();
				int hashCode1 = ((Object) serializedObjIn.get(i)).hashCode();

				for (int j = 0; j < count; j++) {
					if (i == j)
						continue;
					String className2 = ((Object) serializedObjIn.get(j))
							.getClass().getSimpleName();
					int hashCode2 = ((Object) serializedObjIn.get(j))
							.hashCode();

					if (className1.equals(className2)) {
						if (((Object) serializedObjIn.get(i))
								.equals(((Object) serializedObjIn.get(j)))) {
							// add it to list and increment if not exist!
							setClassMapping(hashCode1, className1);
						} else {
							setClassMapping(hashCode1, className1);
							setClassMapping(hashCode2, className2);
						}
					}
				}
			}
		} catch (Exception e) {
			Debug.printDebug(2,
					"ERROR: During comparing the unique Objects from the list of objects "
							+ "found during serialization.");
		} finally {

		}

	}

	/**
	 * @return the inputMapping
	 */
	public HashMap<String, Class<?>> getInputMapping() {
		return inputMapping;
	}

	/**
	 * @param inputMapping
	 *            the inputMapping to set
	 */
	public void setInputMapping() {
		inputMapping.put("int", int.class);
		inputMapping.put("string", String.class);
		inputMapping.put("double", double.class);
		inputMapping.put("long", long.class);
		inputMapping.put("char", char.class);
		inputMapping.put("float", float.class);
		inputMapping.put("short", short.class);
	}

	/**
	 * @return the classMapping
	 */
	public HashMap<Integer, String> getClassMapping() {
		return classMapping;
	}

	/**
	 * @param classMapping
	 *            the classMapping to set
	 */
	public void setClassMapping(Integer value, String className) {

		if (classMapping.get(value) == null) {
			classMapping.put(value, className);
			setUniqueCount(className, 1);
		}
	}

	public HashMap<String, Integer> getUniqueCount() {
		return uniqueCount;
	}

	public void setUniqueCount(String className, Integer value) {
		if (uniqueCount.get(className) != null) {
			value = 1 + uniqueCount.get(className);
			uniqueCount.put(className, value);
		} else if (uniqueCount.get(className) == null)
			uniqueCount.put(className, value);
	}

	/***
	 * 
	 * Overriding toString() method for Helper class
	 */
	@Override
	public String toString() {
		String output = "";
		Iterator<Entry<String, Integer>> it = uniqueCount.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
					.next();
			output = output + "Unique " + pairs.getKey() + "="
					+ pairs.getValue() + "\n";
			it.remove();
		}

		/*
		 * System.out.println("-----");
		 * 
		 * Iterator<Entry<Integer,String>> it1 =
		 * classMapping.entrySet().iterator(); while (it1.hasNext()) {
		 * Map.Entry<Integer,String> pairs = (Map.Entry<Integer,String>) it1
		 * .next(); output = output + "\n" + pairs.getKey() + " = " +
		 * pairs.getValue(); it1.remove(); }
		 */

		return output;
	}
}
