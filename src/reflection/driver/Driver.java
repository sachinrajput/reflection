package reflection.driver;

import java.util.ArrayList;

import reflection.serDeser.Deserialize;
import reflection.serDeser.DeserializeStrategy;
import reflection.serDeser.Serialize;
import reflection.serDeser.SerializeStrategy;
import reflection.util.Helper;
import reflection.util.Debug;

/***
 * Assignment 3 - Use of Reflection
 * @author Sachin Rajput
 *
 */
public class Driver {
	private static int debugValue = 0;	//Default Debug Value
	public static String inputFileName = "";
	public static String outputFileName = "";
	
	/***
	 * Main 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			debugValue = (args[2].isEmpty()) ? debugValue : Integer.parseInt(args[2]);
		} catch (Exception e) {
			Debug.checkInput(args.length,args);
		} 
		
		Debug.setDebugValue(debugValue);
		Debug.checkInput(args.length,args);
		
		inputFileName = args[0];
		outputFileName = args[1];
		
		DeserializeStrategy deSerialize = new Deserialize();
		ArrayList<Object> deSerializedObj = deserializeInput(inputFileName,deSerialize);
		
		Helper helper = new Helper();
		helper.checkUnique(deSerializedObj);
		Debug.printDebug(0, helper.toString());
		
		SerializeStrategy serialize = new Serialize();
		serializeInput(deSerializedObj,serialize);
	}
	
	/***
	 * Method deserializeInput is used to turn the XML text file into class objects with data
	 * @param inputFileNameIn
	 * @param strategyIn
	 * @return
	 */
	public static ArrayList<Object> deserializeInput( String inputFileNameIn,DeserializeStrategy strategyIn ) {
		return strategyIn.deserializeFile(inputFileNameIn);
	}
	
	/***
	 * Method serializeInput is used to turn class objects with data into XML text file
	 * @param serializedObjIn
	 * @param strategyIn
	 */
	public static void serializeInput( ArrayList<Object> serializedObjIn,SerializeStrategy strategyIn ) {
		strategyIn.serializeFile(serializedObjIn);
	}
}
